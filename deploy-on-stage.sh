#!/usr/bin/env bash

HOST=app@node-16.rosti.cz
PORT=16689

# build static version of site
npm run generate

# kill script if build file does not exist
if [[ ! -f ./dist/index.html ]]; then
    echo -e "\e[7;49;32m[ EXIT ]\e[0m \e[1m dist/index.html not found! Killing script. \e[0m"
    exit 1
fi

echo -e "\e[7;49;32m[ ZIP BUILD ]\e[0m \e[1mZip build files and upload them to server \e[0m"
# zip build content
cd dist && tar cvz -f stage.tar.gz * && mv stage.tar.gz .. && cd ..
# send it to the server
scp -P $PORT -C stage.tar.gz $HOST:/tmp/
# remove local zip
rm stage.tar.gz

echo -e "\e[7;49;32m[ SERVER ]\e[0m \e[1mLogin to the server \e[0m"
ssh $HOST -p $PORT <<'ENDSSH'
HOMEDIR=/srv/app/stage.semecky.eu
cd $HOMEDIR
echo -e "\e[7;49;32m[ ASSETS ]\e[0m \e[1mUnzip assets in /tmp/ \e[0m"
mkdir -p /tmp/release/
tar xvzf /tmp/stage.tar.gz -C /tmp/release/
echo -e "\e[7;49;32m[ ASSETS ]\e[0m \e[1mReplace assets \e[0m"
rm -fr $HOMEDIR/public/*
mv /tmp/release/* $HOMEDIR/public
rm -f /tmp/stage.tar.gz
rm -fr /tmp/release
echo -e "\e[7;49;32m[ ASSETS ]\e[0m \e[1mDONE \e[0m"
ENDSSH
ssh $HOST -p $PORT "echo `date '+%Y-%m-%d %H:%M:%S'` nuxt_template deployed on stage.semecky.eu, commit $BITBUCKET_COMMIT >> /srv/app/release.log"
