export const state = () => ({
  locales: process.env.LOCALES || ['en'],
  locale: process.env.LOCALE || 'en'
})

export const mutations = {
  SET_LANG (state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = locale
    }
  }
}
