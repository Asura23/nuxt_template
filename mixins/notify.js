import Vue from "vue";

export default {
  methods: {
    notify(message, type = "info", header = "", duration = 5000) {
      type = ["info", "success", "warn", "error"].includes(type) ? type : "info";
      Vue.notify({
        type: type,
        duration: duration,
        title: header.length > 0 ? this.$t(header) : this.$t("notify.header"),
        text: this.$t(message)
      });
    }
  }
};
