import Vue from "vue";
import VueI18n from "vue-i18n";

Vue.use(VueI18n);

/**
 * I18n (internationalization)
 * allow test localization, either by URL or by selecting language (stored in session)
 */
export default ({ app, store }) => {
  const messages = {};
  const locales = process.env.LOCALES || ['en'];
  locales.forEach(locale => {
    messages[locale] = require("~/locales/" + locale + ".json");
  });

  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch
  app.i18n = new VueI18n({
    locale: store.state.locale,
    fallbackLocale: process.env.LOCALE || 'en',
    messages
  })

  app.i18n.path = (link) => {
    if (app.i18n.locale === app.i18n.fallbackLocale) {
      return `/${link}`
    }

    return `/${app.i18n.locale}/${link}`
  }
}
