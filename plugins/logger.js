/**
 * Logger
 * log messages to a file using simple php api /static/api/Log.php
 *
 * Usage example:
 * this.$log("Not enough cookies!", "warning");
 */
export default ({ $axios }, inject) => {
  const log = function(message, type = "info") {
    // parse message
    const parsed_message = String(message)

    // parse type input
    let parsed_type = type.toLowerCase().match(/^[a-z].*$/);
    parsed_type = parsed_type !== null ? parsed_type[0] : "info"

    // save data from json
    if (process.env.NODE_ENV === "development") {
      // in dev mode log to console
      if (parsed_type === "error") {
        console.error(
          `Logger.log() /logs/${parsed_type}.log`,
          parsed_message
        )
      } else {
        console.log(
          `%cLogger.log() /logs/${parsed_type}.log`,
          "background: #108775; color: #FFFFFF, border-radius: 10px; padding: 2px 8px;",
          parsed_message
        )
      }
    }

    // log to file
    const api_url =
      process.env.NODE_ENV === "development"
        ? "http://127.0.0.1/api/Log.php"
        : "/api/Log.php"

    // log to file
    $axios.request({
      method: "post",
      url: api_url,
      data: {
        message: parsed_message,
        log_file: `${parsed_type}.log`,
        log_ip: true,
      },
      load: false,
    })
  }
  // Inject $log(message, type) in Vue, context and store.
  inject("log", log);
}
