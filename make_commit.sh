#!/usr/bin/env bash

## Automatic commit
# Run this script to create new commit and push it to the repository

# start php localhost if not running for unit tests
echo -e "\e[1;49;34mChecking your work\e[0m"
echo -en "[1/3] reloading local PHP server for unit tests             "
kill -9 `nohup ps &> /dev/null | grep php | grep -oE '^\s*[0-9]+' 2>&1 &` &> /dev/null
rm php_localhost.log
nohup php -S 127.0.0.1:80 -t public/ &> php_localhost.log 2>&1 &
sleep 2s
echo -en "\r[2/3] running unit tests (this may take some time)          "

# run unit tests
npm run test:unit &> unit_test_result.log
if grep -Fq FAIL unit_test_result.log; then
    echo ""
    cat unit_test_result.log
    echo ""
    echo -e "\r\e[1;49;31mError: \e[0m \e[1mThere are some issues to fix!\e[0m"
    echo ""
	exit
else
    echo -e "\r\e[1;49;36m- unit tests finished successfully\e[0m                          "
fi
sleep 1s
rm unit_test_result.log
echo -en "[3/3] running prettier                                      "

## run Prettier
STATUS=$(npm run lint)
SEARCH="issues found"
if [[ "$STATUS" =~ .*"$SEARCH".* ]]; then
	echo -e "\r\e[1;49;31mError: \e[0m \e[1mThere are some issues to fix:\e[0m"
    npm run lint
    exit
fi
echo -e "\r\e[1;49;36m- code is formatted properly\e[0m"

echo -en "\e[1;49;34mEnter commit message:\e[0m "
read answer
if [[ -z "$answer" ]]; then
    # do not commit, when no message provided
    echo ""
    echo -e "\e[1;49;31mNo message provided, cancelling commit!\e[0m"
else
    # pull changes at first and look for merge conflicts
    STATUS=$(git pull 2>&1)
    SEARCH='conflict'
    if [[ "$STATUS" =~ .*"$SEARCH".* ]]; then
        # conflict found!
        git status
        echo ""
        echo -e "\e[1;49;31mThere are merge conflicts!\e[0m"
    else
        # create and push commit
        git add -A
        git commit -m "$answer"
        git push
        echo ""
        echo -e "\e[1;49;34mNew commit created :)\e[0m"
    fi
fi
