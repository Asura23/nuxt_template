# nuxt-project

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

# project notes

It uses simple custom PHP API to handle files on the server. 

## Auth Module

> **Requirements:** axios and VueX (store)

1) run `npm install @nuxtjs/auth`

2) add to `nuxt.config.js`
```javascript
modules: [
  '@nuxtjs/auth'
],
auth: {
  // Options
}
```

3) setting per route
```javascript
export default {
  middleware: 'auth'
}
```

global setting in `nuxt.config.js`
```javascript
router: {
  middleware: ['auth']
}
```
