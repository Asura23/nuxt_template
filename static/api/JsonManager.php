<?php
include('API.php');

/**
 * Class JsonManager
 * Simple PHP class, that will process Vue request and save data to local JSON file
 * Expected POST with data:
 * {
 *  "token" => "Static token, that is used for something like permission check"
 *  "file" => "Filename in public/data folder, that should be updated, only .json allowed"
 *  "data" => "Parsed JSON object that will be saved to file"
 * }
 */
class JsonManager extends API
{

    /**
     * @var bool
     */
    protected $permission;

    /**
     * @var string
     */
    protected $data_folder;

    /**
     * @var string
     */
    protected $backup_folder;

    /**
     * @var string
     */
    protected $token;

    /**
     * Constructor, define basic values
     * JsonManager constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->permission = false;
        $this->data_folder = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data';
        $this->backup_folder = $this->data_folder . DIRECTORY_SEPARATOR . 'backup';
        $this->token = "HtoRVeGaeHzJX5XikdqUCuJXcuPfbMsFRoPlK0Pxv1raYKHy6T2uDDStMX9tsTI6";
    }

    /**
     * Only public function for now,
     * initialize the whole process
     *
     * @return false|string
     */
    public function execute()
    {

        // load data
        $this->loadData();

        // all checks
        if ($this->checkPermission() &&
            $this->validateFilename() &&
            $this->validateData()) {

            // save to file
            $this->saveData();

        }

        // result
        return $this->returnResponse();
    }

    /**
     * Load all data from POST and save them to variable
     *
     * @return $this
     */
    protected function loadData()
    {
        // get data from POST
        if (empty($this->data)) {
            $this->data = json_decode(file_get_contents('php://input'), true)
                ? json_decode(file_get_contents('php://input'), true)
                : [];
        }

        // strip tags from actual data input
        if (!empty($this->data['data'])) {
            $this->data['data'] = json_decode(strip_tags(json_encode($this->data['data'])));
        }

        return $this;
    }

    /**
     * Create backup if necessary (only one per day),
     * limited to 10 last backups
     *
     * @return $this
     */
    protected function createBackup()
    {
	    $data_file = $this->data_folder . DIRECTORY_SEPARATOR . $this->data['file'];
		$backup_file = $this->backup_folder . DIRECTORY_SEPARATOR . $this->data['file'] . '_backup-' . date('Y-m-d');
		// check if backup folder exist, and create one
		if (!file_exists($this->backup_folder)) {
            mkdir($this->backup_folder);
        }
		// if there is no todays backup, make one
		if (!file_exists($backup_file)) {
            copy($data_file, $backup_file);
        }
        // count backups
        $all_backups = scandir($this->backup_folder);
        $current_backups = [];
        foreach ($all_backups as $backup) {
            if(strpos($backup, $this->data['file'] . '_backup') !== false) {
                $current_backups[] = $backup;
            }
        }
        // keep only last 10 backups
        if(count($current_backups) > 10) {
            // delete the oldest record
            unlink($this->backup_folder . DIRECTORY_SEPARATOR . array_shift($current_backups));
        }

		return $this;
	}

    /**
     * Permissions check, currently using simple pre-defined token
     *
     * @return bool
     */
    protected function checkPermission()
    {
        if ($this->permission === true) {
            // already checked
            return true;
        } else {
            // no token
            if (isset($this->data['token'])) {
                $token = $this->data['token'] ? trim($this->data['token']) : null;
                if ($token === $this->token) {
                    $this->permission = true;

                    return true;
                }
            }
            $this->setResponse('Forbidden access', 403);

            return false;
        }

    }

    /**
     * Validate inputted filename
     *
     * @return bool
     */
    protected function validateFilename()
    {

        // name not defined
        if (empty($this->data['file'])) {
            $this->setResponse('Filename not defined', 400);

            return false;
        }

        $parts = explode('.', trim($this->data['file']));

        // not valid json
        if (count($parts) !== 2) {
            $this->setResponse('Invalid filename', 400);

            return false;
        }
        if ($parts[1] !== 'json') {
            $this->setResponse('Filename is not JSON', 400);

            return false;
        }

        return true;
    }

    /**
     * Validate inputted data
     *
     * @return bool
     */
    protected function validateData()
    {

        // data not defined
        if (empty($this->data['data'])) {
            $this->setResponse('Data not defined', 404);

            return false;
        }

        // try create JSON
        $ob = json_decode(json_encode($this->data['data']));
        if ($ob === null) {
            $this->setResponse('Invalid data', 400);

            return false;
        }

        return true;
    }

    /**
     * Saving new data to JSON file
     *
     * @return $this
     */
    protected function saveData()
    {
        $this->createBackup();
        $data_file = $this->data_folder . DIRECTORY_SEPARATOR . $this->data['file'];
        $fp = fopen($data_file, 'w');
        fwrite($fp, json_encode($this->data['data']));
        fclose($fp);
        $this->setResponse('New data have been saved to a file.', 200);

        return $this;
    }

}

$jsonManager = new JsonManager();
echo $jsonManager->execute();;
