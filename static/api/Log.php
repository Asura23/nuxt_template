<?php
include('API.php');

/**
 * Class Log
 * Simple PHP class, that will save messages to log file
 *
 * Expected POST with data:
 * {
 *  "message" => "error message",
 *  "log_file" => "file name in logs folder, optional",
 *  "log_ip" => "boolean, if we should odd user IP to message, optional, default false"
 * }
 *
 */
class Log extends API
{

    /**
     * @var string
     */
    protected $log_folder;

    /**
     * @var string
     */
    protected $log_file;

    /**
     * @var boolean
     */
    protected $log_ip;

    /**
     * Constructor, define basic values
     * Log constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->log_folder = dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'logs';
        $this->log_file = "info.log";
        $this->log_ip = false;
    }

    /**
     * Load all data from POST and save them to variable
     *
     * @return $this
     */
    protected function loadData()
    {
        if (empty($this->data)) {
            $this->data = json_decode(file_get_contents('php://input'), true) ? json_decode(
                file_get_contents('php://input'),
                true
            ) : array();
        }

        // if different filename defined
        if (!empty($this->data['log_file'])) {
            $this->log_file = $this->data['log_file'];
        }

        // if we should log IP address
        if (!empty($this->data['log_ip'])) {
            $this->log_ip = $this->data['log_ip'];
        }

        return $this;
    }

    /**
     * Validate inputted data
     *
     * @return bool
     */
    protected function validateData()
    {

        // check if log folder exists and create it if not
        if (!file_exists($this->log_folder)) {
            mkdir($this->log_folder);
        }

        // if data not defined, return error
        if (empty($this->data['message'])) {
            $this->setResponse('Data not defined', 404);
            return false;
        }

        return true;
    }

    /**
     * Saving log to file
     *
     * @return $this
     */
    protected function saveData()
    {
        $log_path = $this->log_folder . DIRECTORY_SEPARATOR . $this->log_file;

        // add date to message
        $message = date('Y-M-D H:i:s', time()) . " | " . strip_tags($this->data['message']);

        // add IP to message
        if ($this->log_ip) {
            $message .= " [IP: " . $this->getUserIP() . "]";
        }

        // save to a file
        $fp = fopen($log_path, 'a');
        fwrite($fp, $message . PHP_EOL);
        fclose($fp);
        $this->setResponse('Message logged.', 200);
        return $this;
    }

    /**
     * Function to get current user IP address
     *
     * @return mixed
     */
    protected function getUserIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        return $_SERVER['REMOTE_ADDR'];
    }

}

$log = new Log();
echo $log->execute();
