<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header(
    'Access-Control-Allow-Headers: Keep-Alive,User-Agent,Access-Control-Allow-Origin,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization'
);
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    exit;
}


/**
 * Abstract Class Json API
 * Simple PHP class, from where you can extend on, when creating custom api calls
 */
abstract class API
{

    /**
     * @var array
     */
    protected $response;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $message;

    /**
     * Constructor, define basic values
     * API constructor.
     */
    public function __construct()
    {
        $this->response = array(
            'message' => 'Not Found',
            'status' => 404
        );
    }

    /**
     * Only public function for now,
     * initialize the whole process
     *
     * @return false|string
     */
    public function execute()
    {

        // load data
        $this->loadData();

        // validation
        if ($this->validateData()) {

            // save to file
            $this->saveData();
        }

        // return response
        return $this->returnResponse();
    }

    /**
     * Validate inputted data
     *
     * @return bool
     */
    protected function validateData()
    {
        // data not defined
        if (empty($this->data['data'])) {
            $this->setResponse('Data not defined', 404);
            return false;
        }

        return true;
    }

    /**
     * Format API response
     *
     * @param string $message
     * @param int $code
     * @return $this
     */
    protected function setResponse($message, $code = 200)
    {
        $this->response['message'] = $message;
        $this->response['status'] = $code;
        return $this;
    }

    /**
     * Saving log to file
     *
     * @return $this
     */
    protected function saveData()
    {
        $this->setResponse('OK');
        return $this;
    }

    /**
     * Return API response
     *
     * @return string
     */
    protected function returnResponse()
    {
        header('Content-Type: application/json');
        if (function_exists('http_response_code')) {
            http_response_code($this->response['status']);
        }
        return json_encode($this->response);
    }

    protected function debug($name, $value)
    {
        if (!empty($_POST['debug'])) {
            $this->response['debug'][$name] = $value;
        }
    }

}
