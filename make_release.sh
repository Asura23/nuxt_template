#!/usr/bin/env bash

## Automatic deployment
# Run this script to create new version in git and merge development branch to master

## VARIABLES
PROJECT=$(git remote -v | grep -m1 -o -E "/.*.\.git")
BRANCH_DEVELOPMENT=develop
BRANCH_PRODUCTION=production

## VERSIONING
LAST_VERSION=$(git describe --tags | grep -o -E "[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,3}")
MAJOR=$(echo "$LAST_VERSION" | grep -o -E "^[0-9]")
MINOR=$(echo "$LAST_VERSION" | grep -o -E "\.[0-9]\." | grep -o -E "[0-9]")
PATCH=$(echo "$LAST_VERSION" | grep -o -E "[0-9]$")
NEW_MAJOR="$(($MAJOR + 1)).0.0"
NEW_MINOR="$MAJOR.$(($MINOR + 1)).0"
NEW_PATCH="$MAJOR.$MINOR.$(($PATCH + 1))"
CURRENT_DATE=$(date +"%Y-%m-%d")
RELEASE_DATE=$(date +"%d.%m.%Y")

## PROCESS OF MAKING NEW VERSION
# you need to define NEW_VERSION variable before executing this function
function release_new_version {
    echo ""
    echo -e "\e[1;49;34mChecking branches\e[0m"
    git fetch --all --tags &> /dev/null
	check_branch_is_up_to_date $BRANCH_DEVELOPMENT
	check_branch_is_up_to_date $BRANCH_PRODUCTION
	echo ""
    echo -e "\e[1;49;34mRunning tests\e[0m"
    run_tests_on_dev
    echo ""
    echo -e "\e[1;49;34mCreating new release\e[0m"
    create_release_commit
    echo ""
    echo -e "\e[1;49;34mPushing new version to production\e[0m"
    merge_branch_one_to_two $BRANCH_DEVELOPMENT $BRANCH_PRODUCTION
    git checkout "$BRANCH_DEVELOPMENT" &> /dev/null
    echo ""
    echo -e "\e[1;49;34mNew version \e[1m$NEW_VERSION\e[1;49;34m has been released\e[0m"
    echo ""
}

function check_branch_is_up_to_date {
    git checkout "$1" &> /dev/null
    git pull &> /dev/null
    STATUS=$(git status 2>&1)
    SEARCH='On branch '$1
    if [[ "$STATUS" =~ .*"$SEARCH".* ]]; then
        echo -e '\e[1;49;36m- branch '$1' is available\e[0m'
    else
        echo ""
        echo -e '\e[1;49;31mError: \e[0m \e[1mUnable to switch to branch '$1'!\e[0m'
        echo ""
		exit
    fi
    SEARCH='branch is up to date'
    if [[ "$STATUS" =~ .*"$SEARCH".* ]]; then
        echo -e '\e[1;49;36m- branch '$1' is up to date\e[0m'
    else
        git status
        echo ""
        echo -e '\e[1;49;31mError: \e[0m \e[1mBranch '$1' it not up to date!\e[0m'
        echo ""
		exit
    fi
    SEARCH='not staged for commit'
    if [[ "$STATUS" =~ .*"$SEARCH".* ]]; then
        git status
        echo ""
        echo -e '\e[1;49;31mError: \e[0m \e[1mUnstaged changes on branch '$1'!\e[0m'
        echo ""
		exit
    fi
}

# Update several files with new version and date
function run_tests_on_dev {
    git checkout "$BRANCH_DEVELOPMENT" &> /dev/null

	# start php localhost if not running for unit tests
	kill -9 `nohup ps &> /dev/null | grep php | grep -oE '^\s*[0-9]+' 2>&1 &` &> /dev/null
	rm php_localhost.log
	nohup php -S 127.0.0.1:80 -t public/ &> php_localhost.log 2>&1 &
	sleep 2s

	# run unit tests
	npm run test:unit &> unit_test_result.log
	if grep -Fq FAIL unit_test_result.log; then
	    echo ""
	    cat unit_test_result.log
	    echo ""
	    echo -e '\e[1;49;31mError: \e[0m \e[1mThere are some issues to fix!\e[0m'
	    echo ""
		exit
	else
	    echo -e '\e[1;49;36m- unit tests finished successfully\e[0m'
	fi
	sleep 1s
	rm unit_test_result.log

    # run linter
    STATUS=$(npm run lint)
	SEARCH='issues found'
    if [[ "$STATUS" =~ .*"$SEARCH".* ]]; then
        echo ""
        echo -e '\e[1;49;31mError: \e[0m \e[1mThere are some issues to fix!\e[0m'
        echo ""
        npm run prettier
		exit
    else
        echo -e '\e[1;49;36m- code validated by Prettier\e[0m'
    fi
}

# Update several files with new version and date
function create_release_commit {
    git checkout "$BRANCH_DEVELOPMENT" &> /dev/null
    # tag with a new version
	STATUS=$(git tag "$NEW_VERSION" 2>&1)
    SEARCH='already exists'
    if [[ "$STATUS" =~ .*"$SEARCH".* ]]; then
        echo ""
        echo -e '\e[1;49;31mError: \e[0m \e[1mVersion '$NEW_VERSION' already exists in repository!\e[0m'
        echo ""
		exit
    else
	    # update files
	    sed -i "s/VUE_APP_PROJECT_VERSION=.*/VUE_APP_PROJECT_VERSION=$NEW_VERSION/g" .env
	    sed -i "s/VUE_APP_PROJECT_RELEASED=.*/VUE_APP_PROJECT_RELEASED=$RELEASE_DATE/g" .env
	    sed -i "s/\/badge\/version-.*/\/badge\/version-$NEW_VERSION-darkgreen\.svg)]()/g" README.md
	    sed -i "s/\/badge\/released-.*/\/badge\/released-$RELEASE_DATE-black\.svg)]()/g" README.md
	    sed -i "0,/\"version\":.*/s//\"version\": \"$NEW_VERSION\",/g" package.json
	    sed -i "0,/\"version\":.*/s//\"version\": \"$NEW_VERSION\",/g" package-lock.json
	    sed -i "s/<lastmod>.*/<lastmod>$CURRENT_DATE<\/lastmod>/g" public/sitemap.xml
	    # commit them
		git add .env &> /dev/null
	    git add README.md &> /dev/null
	    git add package.json &> /dev/null
	    git add package-lock.json &> /dev/null
	    git add public/sitemap.xml &> /dev/null
	    git commit -m "v.$NEW_VERSION" &> /dev/null
        git push &> /dev/null
        git push --tags &> /dev/null
        echo -e '\e[1;49;36m- created new tag '$NEW_VERSION'\e[0m'
    fi
}

# Pull changes to branch
function merge_branch_one_to_two {
	git checkout "$2" &> /dev/null
    STATUS=$(git pull origin "$1" 2>&1)
    SEARCH='conflict'
    if [[ "$STATUS" =~ .*"$SEARCH".* ]]; then
        echo ""
        echo -e "\e[1;49;31mError: \e[0m \e[1mThere are merge conflicts! Please fix them and then create release manually! \e[0m"
        echo ""
		git reset --hard HEAD
		exit
    else
        # push to new branch
        git push &> /dev/null
        echo -e '\e[1;49;36m- merged changes from branch '$1' to '$2'\e[0m'
    fi
}

## START OF THE SCRIPT
echo -e "\e[1;49;34mCreating new release for\e[0m $PROJECT"
echo -e "Current version: $LAST_VERSION \e[0m"
echo ""

PS3='What version do you want to publish? '
options=("New patch $NEW_PATCH" "New minor $NEW_MINOR" "New major $NEW_MAJOR" "Don't do anything")
select opt in "${options[@]}"
do
case $opt in
"New patch $NEW_PATCH")
    NEW_VERSION="$NEW_PATCH"
    release_new_version
    break
    ;;
"New minor $NEW_MINOR")
    NEW_VERSION="$NEW_MINOR"
    release_new_version
    break
    ;;
"New major $NEW_MAJOR")
    echo ""
    echo -e -n "\e[1mDo you really want to publish MAJOR version (y/n)?\e[0m "
    read answer
    if echo "$answer" | grep -iq "^y" ;then
        NEW_VERSION="$NEW_MAJOR"
        release_new_version
        break
    else
        echo "Cancelling Major release"
        echo ""
    fi
    ;;
"Don't do anything")
    echo ""
    echo "Doing exactly nothing, bye :)"
    echo ""
    break
    ;;
*) echo invalid option;;
esac
done
