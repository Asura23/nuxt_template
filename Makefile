## DEFAULT
default:
	@echo -e "\n\e[7;49;32m MAKE \e[0m \e[1mList of commands \e[0m\n"
	@printf "Usage: make [\e[1;49;32maction\e[0m] \n\nList of actions: \n  \e[1;49;32mdev\e[0m, \e[1;49;32mwatch\e[0m       Build for development and run local server \n  \e[1;49;32mprod\e[0m, \e[1;49;32mbuild\e[0m      Build for production in dist/ directory \n  \e[1;49;32minstall\e[0m, \e[1;49;32mi\e[0m       Install Node Modules (vendors) \n  \e[1;49;32mupdate\e[0m, \e[1;49;32mu\e[0m        Update Node Modules (vendors) to new version \n  \e[1;49;32mserver\e[0m, \e[1;49;32ms\e[0m        Pull latest git changes, install vendors and start localhost server \n  \e[1;49;32mtest\e[0m             Run functional tests \n  \e[1;49;32mprettier\e[0m         Run prettier linter \n  \e[1;49;32mcommit\e[0m, \e[1;49;32mc\e[0m        Create new commit and push it to the repository \n  \e[1;49;32mrelease\e[0m, \e[1;49;32mr\e[0m       Create new version with tags, and merge it to the master branch \n"

## WEBPACK DEV
dev:
	@echo -e "\n\e[7;49;32m DEV \e[0m \e[1mbuild for development and watch file changes\e[0m"
	@npm run serve

## WEBPACK PROD
prod:
	@echo -e "\n\e[7;49;32m PROD \e[0m \e[1mbuild for production\e[0m"
	@npm run build

build: prod

test:
	@echo -e "\n\e[7;49;32m TEST \e[0m \e[1mrun functional tests\e[0m"
	@npm run test:unit

prettier:
	@echo -e "\n\e[7;49;32m prettier \e[0m \e[1mrun prettier test\e[0m"
	@npm run prettier

lc:
	@echo -e "\n    \e[7;49;32m[= LC =]\e[0m \e[1mLicense Checker \e[0m\n"
	@license-checker --production --exclude MIT,BSD,ISC,CC0-1.0,Apache-2.0 --csv

## RE-INSTALL ALL VENDORS
install:
	@echo -e "\n\e[7;49;32m INSTALL \e[0m \e[1m(re)installing all vendors, node_modules\e[0m"
	@echo -e "\n    \e[7;49;32m[= MAKE =]\e[0m \e[1m(Re)Installing all vendors \e[0m\n"
	@echo -e "- removing node_modules"
	@rm -rf node_modules
	@rm -rf dist
	@echo -e "- installing new node_modules"
	@npm install
	@echo -e "- running build"
	npm run build

i: install

## UPDATE VENDORS
update:
	@echo -e "\n\e[7;49;32m UPDATE \e[0m \e[1mtry to update all vendors, node_modules\e[0m"
	@echo -e "- removing node_modules and lock file"
	@rm -rf node_modules
	@rm -rf dist
	@rm package-lock.json
	@echo -e "- installing new node_modules"
	@npm install
	@echo -e "- running build"
	npm run build

u: update

## SYMFONY LOCALHOST SERVER
server:
	@echo -e "\n\e[7;49;32m SERVER \e[0m \e[1mstarting localhost server\e[0m"
	@echo -e "\e[1;49;36m- get latest changes from git repository\e[0m"
	@git pull --rebase --autostash
	@echo -e "\e[1;49;36m- clearing /logs\e[0m"
	@rm -rf ./logs
	@echo -e "\e[1;49;36m- checking for vendor update (Node Modules)\e[0m"
	@npm install
	@nohup php -S 127.0.0.1:80 -t static/ > php_localhost.log 2>&1 &
	@echo -e "\e[1;49;36m- starting PHP localhost on http://127.0.0.1:80/\e[0m"
	@sleep 2s
	@echo -e "\e[1;49;36m- running jest tests\e[0m"
	@npm run test
	@npm run dev

s: server

kill_php:
	@echo -e "\n\e[7;49;32m KILL_PHP \e[0m \e[1mkilling localhost PHP server\e[0m"
	@kill -9 `nohup ps | grep php | grep -oE '^\s*[0-9]+'`
	@rm php_localhost.log
	@rm -rf /public/logs

kp: kill_php
ks: kill_php

kill_docker:
	@echo -e "\n\e[7;49;32m KILL_DOCKER \e[0m \e[1mkilling all Docker containers\e[0m"
	@docker-compose down
	@sleep 2s
	@docker stop $$(docker ps -aq)
	@sleep 2s
	@docker rm $$(docker ps -aq)

kd: kill_docker

## AUTOMATIZATION

release:
	@./make_release.sh

r: release

commit:
	@./make_commit.sh

c: commit
